# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.5] - 2020-11-22
### Fixed
- Fix missing closing brace in AP Top News generator

## [0.1.4] - 2020-11-22
### Added
- GitHub Releases feed

### Fixed
- Support multiple images in False Knees generator

## [0.1.3] - 2020-11-16
### Added
- Twitter
- Twitter (via API v2)

### Fixed
- Allow pressing enter key in forms


## [0.1.2] - 2020-11-09
### Added
- Instagram user feed

### Fixed
- RSS Channel generator attribute

## [0.1.1] - 2020-11-08
### Added
- False Knees feed

### Fixed
- Use HTTP HEAD method to fetch headers for enclosures.
- Change feeds to only request news listing & not individual articles when
  possible.
- Change feeds to move images into description from enclosure.

## [0.1.0] - 2020-11-08
Initial release.
