document.querySelectorAll("form").forEach(convertFormButtonToLink);

function convertFormButtonToLink(form) {
  const submitButton = form.querySelector("input[type=\"submit\"]");

  const rssLogo = document.getElementById("rss-logo");

  const submitLink = document.createElement("a");
  submitLink.appendChild(cloneSvg(rssLogo));
  submitLink.appendChild(document.createTextNode(" " + submitButton.value));
  submitLink.className = submitButton.className;

  const action = form.action;

  submitButton.parentNode.insertBefore(submitLink, submitButton);
  submitButton.parentNode.removeChild(submitButton);

  function updateLink() {
    const queryString = new URLSearchParams(new FormData(form)).toString();
    const url = action + "?" + queryString
    submitLink.href = url;
  }
  updateLink();

  form.addEventListener("change", e => {
    updateLink();
  });
}

function cloneSvg(svg) {
  const clone = svg.cloneNode();
  clone.innerHTML = svg.innerHTML;
  return clone;
}
