defmodule HTML do
  @moduledoc false

  require Meeseeks.CSS

  def title(document) do
    document
    |> Meeseeks.one(Meeseeks.CSS.css("title"))
    |> Meeseeks.text()
  end

  def meta_description(document) do
    Meeseeks.one(document, Meeseeks.CSS.css("meta[name=\"description\"]"))
    |> Meeseeks.attr("content")
  end

  def meta_refresh(document) do
    refresh =
      document
      |> Meeseeks.one(Meeseeks.CSS.css("meta[http-equiv=\"refresh\"]"))
      |> Meeseeks.attr("content")

    [_full_match, delay_seconds, url] = Regex.run(~r/(\d+);\s*url=(.*)/i, refresh)
    {String.to_integer(delay_seconds), url}
  end
end
