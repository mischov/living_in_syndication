defmodule Feeds.EbirdOrg do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "eBird",
        url: "https://ebird.org"
      }

  require Meeseeks.CSS

  @news_listing_url "https://ebird.org/news"

  @impl RssGenerator
  def get_channel(_params) do
    document = get_document(@news_listing_url)

    title = HTML.title(document)

    items =
      document
      |> Meeseeks.all(Meeseeks.CSS.css("figure.Media"))
      |> Enum.map(&extract_item/1)

    pub_date = Enum.map(items, & &1.pubDate) |> Enum.max()

    %Rss.Channel{
      title: title,
      link: @news_listing_url,
      description: HTML.meta_description(document),
      language: "en-us",
      pubDate: pub_date,
      lastBuildDate: pub_date,
      image: Rss.Image.from_link_rel_icon(document, title, @news_listing_url),
      items: items
    }
  end

  defp extract_item(queryable) do
    link = Meeseeks.one(queryable, Meeseeks.CSS.css(".Heading a"))
    title = Meeseeks.text(link)
    url = "https://ebird.org" <> Meeseeks.attr(link, "href")

    description =
      Meeseeks.one(queryable, Meeseeks.CSS.css(".Blurb-excerpt"))
      |> Meeseeks.text()

    pub_date =
      Meeseeks.one(queryable, Meeseeks.CSS.css(".Blurb-meta span:last-child"))
      |> Meeseeks.text()
      |> Timex.parse!("{Mfull} {D}, {YYYY}")
      |> Timex.to_datetime("America/New_York")

    image =
      Meeseeks.one(queryable, Meeseeks.CSS.css("img.ImageResponsive"))
      |> Meeseeks.attr("data-src")

    description_with_image = """
      <p>#{description}</p>
      <p><img src="#{image}" alt="#{title}" /></p>
    """

    %Rss.Item{
      title: title,
      link: url,
      description: description_with_image,
      guid: url,
      pubDate: pub_date
    }
  end

  defp get_document(url) do
    url
    |> HTTP.Client.get_body!()
    |> Meeseeks.parse()
  end
end
