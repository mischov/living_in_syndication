defmodule Feeds.AudubonOrg do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "National Audubon Society",
        url: "https://www.audubon.org"
      }

  require Meeseeks.CSS

  @news_listing_url "https://www.audubon.org/menu/news"

  @impl RssGenerator
  def get_channel(_params) do
    document = get_document(@news_listing_url <> "?page=1")

    open_graph = HTML.OpenGraph.extract_from_html(document)

    items =
      document
      |> Meeseeks.all(Meeseeks.CSS.css(".editorial-card"))
      |> Enum.map(&extract_item/1)

    date = Enum.map(items, & &1.pubDate) |> Enum.max()

    image =
      document
      |> Meeseeks.one(Meeseeks.CSS.css("link[rel=\"shortcut icon\"]"))
      |> Meeseeks.attr("href")

    %Rss.Channel{
      title: open_graph["og:site_name"],
      link: @news_listing_url,
      description: "News from " <> open_graph["og:site_name"],
      language: "en-us",
      pubDate: date,
      lastBuildDate: date,
      image:
        %Rss.Image{
          url: image,
          title: open_graph["og:site_name"],
          link: @news_listing_url
        }
        |> Rss.Image.constrain_dimensions(),
      items: items
    }
  end

  defp extract_item(queryable) do
    link = Meeseeks.one(queryable, Meeseeks.CSS.css(".editorial-card-title a"))
    title = Meeseeks.text(link)
    url = "https://www.audubon.org" <> Meeseeks.attr(link, "href")

    description =
      Meeseeks.one(queryable, Meeseeks.CSS.css(".editorial-card-body"))
      |> Meeseeks.text()

    pub_date =
      Meeseeks.one(queryable, Meeseeks.CSS.css(".editorial-card-dateline .date-display-single"))
      |> Meeseeks.text()
      |> Timex.parse!("{Mfull} {D}, {YYYY}")
      |> Timex.to_datetime("America/New_York")

    image =
      Meeseeks.one(queryable, Meeseeks.CSS.css(".editorial-card-photo img"))
      |> Meeseeks.attr("src")

    description_with_image = """
    <p>#{description}</p>
    <p><img src="#{image}" alt="#{title}" /></p>
    """

    %Rss.Item{
      title: title,
      link: url,
      description: description_with_image,
      guid: url,
      pubDate: pub_date
    }
  end

  defp get_document(url) do
    url
    |> HTTP.Client.get_body!()
    |> Meeseeks.parse()
  end
end
