defmodule Feeds.GitHubComReleases do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "GitHub Releases",
        url: "https://www.github.com",
        params: [
          %RssGenerator.Param{
            name: "owner",
            type: "text",
            description: "The GitHub repo owner (user/organization)"
          },
          %RssGenerator.Param{
            name: "repo",
            type: "text",
            description: "The GitHub repository name"
          }
        ]
      }

  @impl RssGenerator
  def get_channel(%{"owner" => owner, "repo" => repo}) do
    %{"description" => description, "full_name" => full_name, "html_url" => url} =
      get_document("repos/#{owner}/#{repo}")

    title = "#{full_name} (#{description}) - GitHub Releases"

    items = get_releases(owner, repo)
    pub_date = Enum.map(items, & &1.pubDate) |> Enum.max()

    %Rss.Channel{
      title: title,
      link: url,
      description: title,
      language: "en-us",
      pubDate: pub_date,
      lastBuildDate: pub_date,
      image:
        %Rss.Image{
          url: "https://github.com/fluidicon.png",
          title: title,
          link: url,
          width: 512,
          height: 512
        }
        |> Rss.Image.constrain_dimensions(),
      items: items
    }
  end

  def get_releases(owner, repo) do
    get_document("repos/#{owner}/#{repo}/releases")
    |> Enum.map(&release_to_item/1)
  end

  defp release_to_item(%{
         "body" => body,
         "html_url" => url,
         "name" => name,
         "published_at" => published_at
       }) do
    {:ok, pub_date, _offset} = DateTime.from_iso8601(published_at)

    %Rss.Item{
      title: name,
      link: url,
      description: Earmark.as_html!(body),
      guid: url,
      pubDate: pub_date
    }
  end

  defp get_document(path) do
    HTTP.Client.get_body!(
      "https://api.github.com/#{path}",
      [{"accept", "application/vnd.github.v3+json"}]
    )
    |> Jason.decode!()
  end
end
