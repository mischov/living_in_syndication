defmodule Feeds.InstagramCom do
  @moduledoc false

  use RssGenerator,
      %RssGenerator.Metadata{
        name: "Instagram",
        url: "https://www.instagram.com",
        params: [
          %RssGenerator.Param{
            name: "user",
            type: "text",
            description: "The Instagram user to fetch posts for"
          }
        ]
      }

  require Meeseeks.CSS

  @impl RssGenerator
  def get_channel(%{"user" => user}) do
    {user_id, full_name, profile_pic_url} = get_user_profile(user)

    title = "#{full_name} (@#{user}) - Instagram"
    profile_url = "https://www.instagram.com/#{user}/"

    items = get_posts(user_id)
    pub_date = Enum.map(items, & &1.pubDate) |> Enum.max()

    %Rss.Channel{
      title: title,
      link: profile_url,
      description: title,
      language: "en-us",
      pubDate: pub_date,
      lastBuildDate: pub_date,
      image:
        %Rss.Image{
          url: profile_pic_url,
          title: title,
          link: profile_url,
          width: 150,
          height: 150
        }
        |> Rss.Image.constrain_dimensions(),
      items: items
    }
  end

  def get_posts(user_id) do
    %{"data" => %{"user" => %{"edge_owner_to_timeline_media" => %{"edges" => edges}}}} =
      HTTP.Client.get_body!(
        "https://www.instagram.com/graphql/query/?query_hash=58b6785bea111c67129decbe6a448951&variables={%22id%22%3A%22#{
          user_id
        }%22%2C%22first%22%3A10}"
      )
      |> Jason.decode!()

    edges
    |> Enum.map(&edge_to_item/1)
  end

  def edge_to_item(
        edge = %{
          "node" => %{"shortcode" => shortcode, "taken_at_timestamp" => taken_at_timestamp}
        }
      ) do
    url = "https://www.instagram.com/p/#{shortcode}/"

    caption = get_caption(edge)
    [title | _] = String.split(caption, "\n")
    pub_date = DateTime.from_unix!(taken_at_timestamp)

    description =
      get_description(edge) <>
        """
        <p>
          #{String.replace(caption, "\n", "<br>")}
        </p>
        """

    %Rss.Item{
      title: title,
      link: url,
      description: description,
      guid: url,
      pubDate: pub_date
    }
  end

  defp get_description(%{
         "node" => %{
           "__typename" => "GraphSidecar",
           "edge_sidecar_to_children" => %{"edges" => edges}
         }
       }) do
    edges
    |> Enum.map(&get_description/1)
    |> Enum.join("\n")
  end

  defp get_description(
         edge = %{
           "node" => %{"__typename" => "GraphImage", "display_url" => display_url}
         }
       ) do
    """
    <p>
      <img src="#{display_url}" alt="#{get_caption(edge)}" />
    </p>
    """
  end

  defp get_description(
         edge = %{
           "node" => %{
             "__typename" => "GraphVideo",
             "display_url" => display_url,
             "video_url" => video_url
           }
         }
       ) do
    """
    <p>
      <video controls>
        <source src="#{video_url}" poster="#{display_url}" type="video/mp4">
        <img src="#{video_url}" alt="#{get_caption(edge)}">
      </video>
    </p>
    """
  end

  defp get_caption(%{
         "node" => %{"edge_media_to_caption" => %{"edges" => [%{"node" => %{"text" => text}}]}}
       }),
       do: text

  defp get_caption(_), do: "(no text)"

  def get_user_profile(user) do
    %{
      "users" => [
        %{
          "user" => %{
            "full_name" => full_name,
            "pk" => user_id,
            "profile_pic_url" => profile_pic_url
          }
        }
        | _
      ]
    } =
      HTTP.Client.get_body!("https://www.instagram.com/web/search/topsearch/?query=#{user}")
      |> Jason.decode!()

    {user_id, full_name, profile_pic_url}
  end
end
