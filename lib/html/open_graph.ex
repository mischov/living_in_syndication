defmodule HTML.OpenGraph do
  @moduledoc false

  require Meeseeks.CSS

  def extract_from_html(document = %Meeseeks.Document{}) do
    document
    |> Meeseeks.all(Meeseeks.CSS.css("meta[property^=\"og:\"]"))
    |> Enum.map(fn tag ->
      {Meeseeks.attr(tag, "property"), Meeseeks.attr(tag, "content")}
    end)
    |> Map.new()
  end
end
