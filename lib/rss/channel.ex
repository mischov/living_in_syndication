defmodule Rss.Channel do
  @moduledoc false

  @enforce_keys [:title, :link, :description]

  @type t() :: %__MODULE__{
          atom_link: String.t() | nil,
          category: String.t() | nil,
          cloud: String.t() | nil,
          copyright: String.t() | nil,
          description: String.t() | nil,
          docs: String.t(),
          generator: String.t(),
          image: Rss.Image.t(),
          items: [Rss.Item.t()],
          language: String.t(),
          lastBuildDate: DateTime.t() | nil,
          link: String.t(),
          managingEditor: String.t() | nil,
          pubDate: DateTime.t() | nil,
          rating: String.t() | nil,
          skipDays: integer() | nil,
          skipHours: integer() | nil,
          textInput: String.t() | nil,
          title: String.t(),
          ttl: String.t() | nil,
          webMaster: String.t() | nil
        }

  defstruct [
    :title,
    :link,
    :description,
    :language,
    :copyright,
    :managingEditor,
    :webMaster,
    :pubDate,
    :lastBuildDate,
    :category,
    :cloud,
    :ttl,
    :image,
    :rating,
    :textInput,
    :skipHours,
    :skipDays,
    :items,
    :atom_link,
    generator: "Living in Syndication",
    docs: "https://www.rssboard.org/rss-specification"
  ]

  def to_xml(channel = %Rss.Channel{}) do
    {:channel, [],
     [
       {:title, [], [channel.title]},
       {:link, [], [channel.link]},
       {:description, [], [channel.description]},
       {:language, [], [channel.language]},
       {:copyright, [], [channel.copyright]},
       {:managingEditor, [], [channel.managingEditor]},
       {:webMaster, [], [channel.webMaster]},
       Rss.DateTime.date_tag(:pubDate, channel.pubDate),
       Rss.DateTime.date_tag(:lastBuildDate, channel.lastBuildDate),
       {:category, [], [channel.category]},
       {:generator, [], [channel.generator]},
       {:docs, [], [channel.docs]},
       # {:cloud, [], [channel.cloud]},
       {:ttl, [], [channel.ttl]},
       Rss.Image.to_xml(channel.image),
       {:rating, [], [channel.rating]},
       # {:textInput, [], [channel.textInput]},
       {:skipHours, [], [channel.skipHours]},
       {:skipDays, [], [channel.skipDays]},
       {:"atom:link", [{:href, channel.atom_link}, {:rel, "self"}], []}
     ]
     |> Enum.concat(Enum.map(channel.items, &Rss.Item.to_xml/1))
     |> Rss.Xml.cleanup_content()}
  end
end
