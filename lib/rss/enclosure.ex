defmodule Rss.Enclosure do
  @moduledoc false

  @enforce_keys [:url, :length, :type]
  defstruct [:url, :length, :type]

  def from_url(url) do
    image_headers = HTTP.Client.get_headers!(url)

    %Rss.Enclosure{
      url: url,
      length: HTTP.Client.get_header(image_headers, "content-length"),
      type: HTTP.Client.get_header(image_headers, "content-type")
    }
  end

  def to_xml(nil) do
    {:enclosure, [], []}
  end

  def to_xml(%Rss.Enclosure{url: url, length: length, type: type}) do
    {:enclosure, [{:url, url}, {:length, length}, {:type, type}], []}
  end
end
