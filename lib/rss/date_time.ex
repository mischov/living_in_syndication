defmodule Rss.DateTime do
  @moduledoc false

  def date_tag(tag, date = %DateTime{}) when is_atom(tag) do
    {tag, [], [format_rfc_822(date)]}
  end

  def date_tag(tag, _) when is_atom(tag) do
    {tag, [], []}
  end

  def format_rfc_822(dt = %DateTime{}) do
    Timex.format!(dt, "%a, %d %b %Y %T %z", :strftime)
  end
end
