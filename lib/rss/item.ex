defmodule Rss.Item do
  @moduledoc false

  @type t() :: %__MODULE__{
          author: String.t(),
          category: String.t(),
          comments: [String.t()],
          description: String.t(),
          enclosure: any,
          guid: String.t(),
          link: String.t(),
          pubDate: DateTime.t(),
          source: String.t(),
          title: String.t()
        }

  defstruct [
    :title,
    :link,
    :description,
    :author,
    :category,
    :comments,
    :enclosure,
    :guid,
    :pubDate,
    :source
  ]

  def to_xml(item = %Rss.Item{}) do
    {:item, [],
     [
       {:title, [], [item.title]},
       {:link, [], [item.link]},
       {:description, [], [item.description]},
       {:author, [], [item.author]},
       # {:category, [], [item.category]},
       {:comments, [], [item.comments]},
       Rss.Enclosure.to_xml(item.enclosure),
       {:guid, [{:isPermaLink, true}], [item.guid]},
       Rss.DateTime.date_tag(:pubDate, item.pubDate)
       # {:source, [], [item.source]},
     ]
     |> Rss.Xml.cleanup_content()}
  end

  def from_open_graph(document) do
    open_graph = HTML.OpenGraph.extract_from_html(document)

    pub_date =
      case open_graph["og:updated_time"] do
        ut when is_binary(ut) ->
          {:ok, date, _offset} = DateTime.from_iso8601(ut)
          date

        _ ->
          nil
      end

    %Rss.Item{
      title: open_graph["og:title"],
      link: open_graph["og:url"],
      description: open_graph["og:description"],
      guid: open_graph["og:url"],
      pubDate: pub_date,
      enclosure: Rss.Enclosure.from_url(open_graph["og:image"])
    }
  end
end
