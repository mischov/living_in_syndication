defmodule Rss.Xml do
  @moduledoc false

  def cleanup_content(content) when is_list(content) do
    content
    |> Enum.reject(&is_empty?/1)
    |> Enum.map(&convert_strings/1)
  end

  defp convert_strings({tag, attrs, [str]}) when is_binary(str),
    do: {tag, attrs, [String.to_charlist(str)]}

  defp convert_strings(elem), do: elem

  defp is_empty?({_tag, [], []}), do: true
  defp is_empty?({_tag, [], [nil]}), do: true
  defp is_empty?(_elem), do: false
end
