defmodule LivingInSyndication.Template do
  @moduledoc false

  @template_dir "lib/living_in_syndication/templates"

  defmacro __using__(_opts) do
    quote do
      require EEx
      import LivingInSyndication.Template, only: [deftemplate: 1]
    end
  end

  defmacro deftemplate(name) do
    fun_name =
      name
      |> String.replace(".", "_")
      |> String.to_atom()

    quote do
      EEx.function_from_file(
        :defp,
        unquote(fun_name),
        LivingInSyndication.Template.template_path(unquote(name)),
        [:assigns]
      )
    end
  end

  def template_path(template) do
    Path.join(@template_dir, template) <> ".eex"
  end
end
