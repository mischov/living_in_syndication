defmodule LivingInSyndication.Router do
  use Plug.Router
  use LivingInSyndication.Template
  deftemplate("index.html")

  plug(Plug.Static, at: "/static", from: :living_in_syndication)
  plug(:match)
  plug(:dispatch)

  get "/" do
    render(conn, &index_html/1, generators: RssGenerator.get_implementors())
  end

  get "/rss/:host" do
    conn = Plug.Conn.fetch_query_params(conn)
    host = conn.path_params["host"]
    rss_path = "/rss/#{host}"

    module =
      RssGenerator.get_implementors()
      |> Enum.find_value(fn {module, metadata} ->
        if RssGenerator.Metadata.rss_path(metadata) == rss_path do
          module
        else
          nil
        end
      end)

    case module do
      nil ->
        send_resp(conn, 404, "404 not found")

      module ->
        channel =
          module.get_channel(conn.query_params)
          |> Map.put(:atom_link, Plug.Conn.request_url(conn))

        xml = Rss.to_xml_string(%Rss{channel: channel})

        conn
        |> put_resp_content_type("application/xml")
        # disabled because firefox tries to download the file
        # |> put_resp_content_type("application/rss+xml")
        |> send_resp(200, xml)
    end
  end

  match _ do
    send_resp(conn, 404, "404 not found")
  end

  defp render(conn = %{status: status}, template, assigns) do
    body = template.(Map.new(assigns))

    conn
    |> put_resp_content_type("text/html; charset=UTF-8")
    |> send_resp(status || 200, body)
  end
end
