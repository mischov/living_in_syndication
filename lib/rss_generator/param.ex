defmodule RssGenerator.Param do
  @moduledoc false

  alias RssGenerator.Param

  @enforce_keys [:name, :type, :description]
  defstruct [:name, :type, :description, required: true]

  @type t() :: %Param{
          name: String.t(),
          type: String.t(),
          description: String.t(),
          required: boolean()
        }

  def as_html(
        param = %Param{name: name, type: "checkbox", description: description},
        metadata = %RssGenerator.Metadata{}
      ) do
    id = html_id(param, metadata)

    """
    <div class="form-group form-check">
      <input type="checkbox"
        class="form-check-input"
        id="#{id}"
        name="#{name}"
        aria-describedby="#{id}Description"
      />
      <label for="#{id}">
        #{name}
      </label>
      <small id="#{id}Description" class="form-text text-muted">
        #{description}
      </small>
    </div>
    """
  end

  def as_html(
        param = %Param{name: name, type: type, description: description, required: required},
        metadata = %RssGenerator.Metadata{}
      ) do
    id = html_id(param, metadata)
    required_attr = if required, do: "required"

    """
    <div class="form-group">
      <label for="#{id}">
        #{name}
      </label>
      <input type="#{type}"
        class="form-control"
        id="#{id}"
        name="#{name}"
        aria-describedby="#{id}Description"
        #{required_attr}
      />
      <small id="#{id}Description" class="form-text text-muted">
        #{description}
      </small>
    </div>
    """
  end

  defp html_id(%Param{name: name}, metadata = %RssGenerator.Metadata{}) do
    String.replace(RssGenerator.Metadata.host(metadata), ".", "-") <> "-" <> name
  end
end
