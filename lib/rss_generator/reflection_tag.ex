defprotocol RssGenerator.ReflectionTag do
  @moduledoc false

  @callback _info(keyword()) :: keyword()
end
